#ifndef CutsHB_OD_AmLi_H
#define CutsHB_OD_AmLi_H

#include "EventBase.h"

class CutsHB_OD_AmLi {

public:
    CutsHB_OD_AmLi(EventBase* eventBase);
    ~CutsHB_OD_AmLi();
    bool HB_OD_AmLiCutsOK();

private:
    EventBase* m_event;
};

#endif
