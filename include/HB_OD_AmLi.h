#ifndef HB_OD_AmLi_H
#define HB_OD_AmLi_H

#include "Analysis.h"

#include "CutsHB_OD_AmLi.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class HB_OD_AmLi : public Analysis {

public:
    HB_OD_AmLi();
    ~HB_OD_AmLi();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsHB_OD_AmLi* m_cutsHB_OD_AmLi;
    ConfigSvc* m_conf;
    double zPosFromRow(double row);

};

#endif
