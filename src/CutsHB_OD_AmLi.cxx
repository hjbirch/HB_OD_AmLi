#include "CutsHB_OD_AmLi.h"
#include "ConfigSvc.h"

CutsHB_OD_AmLi::CutsHB_OD_AmLi(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsHB_OD_AmLi::~CutsHB_OD_AmLi()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsHB_OD_AmLi::HB_OD_AmLiCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
