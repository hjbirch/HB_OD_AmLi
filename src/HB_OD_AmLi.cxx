#include "HB_OD_AmLi.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsHB_OD_AmLi.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include <THStack.h>

#include <math.h>   /* sin */
#include <stdlib.h> /* abs */
#define PI 3.14159265359

#include <sstream>

// For writing csv files for Paraview
#include "VisSvc.h"
#include <iostream>
#include <fstream>

// Constructor
HB_OD_AmLi::HB_OD_AmLi()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // m_event->IncludeBranch("pulsesTPCHG");  // Not doing TPC coincidence stuff right now
    m_event->IncludeBranch("pulsesSkin");
    m_event->IncludeBranch("pulsesODHG");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("HB_OD_AmLi Analysis");

    // Setup the analysis specific cuts.
    m_cutsHB_OD_AmLi = new CutsHB_OD_AmLi(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();

    //For Paraview
    m_vis = new VisSvc(m_event);
}

// Destructor
HB_OD_AmLi::~HB_OD_AmLi()
{
    delete m_cutsHB_OD_AmLi;
}

bool doVis = true;
ofstream OD_posFile;
//ofstream Skin_posFile;
//ofstream TPCTop_posFile;
//ofstream TPCBottom_posFile;

bool isFirstEvent = true;
double firstEventTime = 0;

// Vectors to store the pulseHitTime per time bin
std::vector<int> pulseHitTime_30;
std::vector<int> pulseHitTime_40;
std::vector<int> pulseHitTime_50;
std::vector<int> pulseHitTime_60;
std::vector<int> pulseHitTime_70;
std::vector<int> pulseHitTime_80;
std::vector<int> pulseHitTime_90;
std::vector<int> pulseHitTime_100;
std::vector<int> pulseHitTime_110;
std::vector<int> pulseHitTime_120;
std::vector<int> pulseHitTime_130;
std::vector<int> pulseHitTime_140;
std::vector<int> pulseHitTime_150;
std::vector<int> pulseHitTime_160;
std::vector<int> pulseHitTime_170;
std::vector<int> pulseHitTime_180;
std::vector<int> pulseHitTime_190;
std::vector<int> pulseHitTime_200;

std::vector<float> FirstLightTheta;
std::vector<float> FirstLightZ;
std::vector<float> FirstLightChPulseArea;

// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void HB_OD_AmLi::Initialize()
{
    INFO("Initializing HB_OD_AmLi Analysis");

    //m_vis->InitParaviewODPMTPos();
    // m_vis->InitParaviewSkinPMTPos();
    // m_vis->InitParaviewTPCPMTPos();

    // if (doVis)
    // {
    //     OD_posFile.open("AmLi_OD.csv", std::ios_base::app);
    //     OD_posFile << "x,y,z,phd\n";

    //     //Skin_posFile.open("AmLi_OD_Skin.csv", std::ios_base::app);
    //     //Skin_posFile << "x,y,z,phd\n";

    //     //TPCTop_posFile.open("AmLi_TPC_top.csv", std::ios_base::app);
    //     //TPCTop_posFile <<  "x,y,z,phd\n";
    //     //
    //     //TPCBottom_posFile.open("AmLi_TPC_botttom.csv", std::ios_base::app);
    //     //TPCBottom_posFile <<  "x,y,z,phd\n";
    // }
}

// Execute() - Called once per event.
void HB_OD_AmLi::Execute()
{
    if (!isFirstEvent)
    {
        firstEventTime = (*m_event->m_eventHeader)->triggerTimeStamp_s + (*m_event->m_eventHeader)->triggerTimeStamp_ns * 1E-9;
        isFirstEvent = false;
    }

    double eventStart = (*m_event->m_eventHeader)->triggerTimeStamp_s + (*m_event->m_eventHeader)->triggerTimeStamp_ns * 1E-9;
    m_hists->BookFillHist("DatasetInfo/eventRelativeStartTime", 2000, 0.0, 2000000, eventStart - firstEventTime);

    double maxBinNS = 1000000;
    double minBinNS = -1000000;
    double nBins = 1000;

    int areaTight = 600;

    int ODMaxPulse = (*m_event->m_odHGPulses)->maxPulseID;
    int SkinMaxPulse = (*m_event->m_skinPulses)->maxPulseID;
    //int TPCMaxPulse = (*m_event->m_tpcHGPulses)->maxPulseID;

    float OD_theta;
    double OD_z;
    double OD_r = 1500; //Radius to set the Paraview output points

    float Skin_theta;
    double Skin_z;
    double Skin_r = 800; //Radius to set the Paraview output points

    // Look at all OD pulses
    for (int i = 0; i < (*m_event->m_odHGPulses)->nPulses; i++)
    {
        if ((*m_event->m_odHGPulses)->coincidence[i] > 5)
        {
            m_hists->BookFillHist("OD/ODPulseArea", 2000, 0.0, 2000, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            m_hists->BookFillHist("OD/ODPulseAreaTight", 2000, 0.0, areaTight, (*m_event->m_odHGPulses)->pulseArea_phd[i]);

            // get theta and z, plot it for all >5 coincidences
            OD_theta = (*m_event->m_odHGPulses)->pulseTheta[i] * PI / 180.;
            OD_z = zPosFromRow((*m_event->m_odHGPulses)->pulseZPosition[i]);
            m_hists->BookFillHist("OD/AllODCentroidPositions", 450, 0., 6.3, 450, -150, 300, OD_theta, OD_z);

            float pulseLadderPos = (*m_event->m_odHGPulses)->pulseLadderPosition[i];
            float pulseZPos = (*m_event->m_odHGPulses)->pulseZPosition[i];
            m_hists->BookFillHist("OD/Ladder_Z", 20, 0.5, 20.5, 6, 0.5, 6.5, pulseLadderPos, pulseZPos);

            //!Uncomment for CSD selection
            // // All OD Pulses > coincidence 5 with a cut and the hottest source tube in the OD
            // if (((OD_theta > 3.8) && (OD_theta < 4.5) && (OD_z > 50) && (OD_z < 100)) || ((OD_theta < 0.4 && OD_theta > 5.8) && (OD_z > 50 && OD_z < 100)) || ((OD_theta < 2.4 && OD_theta > 1.7) && (OD_z > 50 && OD_z < 100)))
            // {
            //     m_hists->BookFillHist("AllSourceTubes/PositionCutPulseArea", 2000, 0.0, 2000, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            //     m_hists->BookFillHist("AllSourceTubes/PositionCutODPulseAreaTight", 2000, 0.0, areaTight, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            //     m_hists->BookFillHist("AllSourceTubes/PositionCutODCentroidPositions", 450, 0., 6.3, 450, -150, 300, OD_theta, OD_z);
            // }

            // // All OD Pulses > coincidence 5 with a cut on CSD 3
            // if (((OD_theta > 3.8) && (OD_theta < 4.6) && (OD_z > 50) && (OD_z < 100)))
            // {
            //     m_hists->BookFillHist("CSD3/PositionCutPulseArea", 2000, 0.0, 2000, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            //     m_hists->BookFillHist("CSD3/PositionCutODPulseAreaTight", 2000, 0.0, areaTight, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            //     m_hists->BookFillHist("CSD3/PositionCutODCentroidPositions", 450, 0., 6.3, 450, -150, 300, OD_theta, OD_z);
            // }

            // // All OD Pulses > coincidence 5 with a cut on CSD 1
            // if (((OD_theta < 0.4) && (OD_theta> 5.8) && (OD_z > 50) && (OD_z < 100)))
            // {
            //     m_hists->BookFillHist("CSD1/PositionCutPulseArea", 2000, 0.0, 2000, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            //     m_hists->BookFillHist("CSD1/PositionCutODPulseAreaTight", 2000, 0.0, areaTight, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            //     m_hists->BookFillHist("CSD1/PositionCutODCentroidPositions", 450, 0., 6.3, 450, -150, 300, OD_theta, OD_z);
            // }

            // // All OD Pulses > coincidence 5 with a cut on CSD 2
            // if (((OD_theta < 2.4) && (OD_theta > 1.7) && (OD_z > 50) && (OD_z < 100)))
            // {
            //     m_hists->BookFillHist("CSD2/PositionCutPulseArea", 2000, 0.0, 2000, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            //     m_hists->BookFillHist("CSD2/PositionCutODPulseAreaTight", 2000, 0.0, areaTight, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
            //     m_hists->BookFillHist("CSD2/PositionCutODCentroidPositions", 450, 0., 6.3, 450, -150, 300, OD_theta, OD_z);
            // }

            //!Uncomment for DD selection
            if (((OD_theta < 4) && (OD_theta > 2.5) && (OD_z > 40) && (OD_z < 150)))
            {
                m_hists->BookFillHist("DD/PositionCutPulseArea", 2000, 0.0, 2000, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
                m_hists->BookFillHist("DD/PositionCutODPulseAreaTight", 2000, 0.0, areaTight, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
                m_hists->BookFillHist("DD/PositionCutODCentroidPositions", 450, 0., 6.3, 450, -150, 300, OD_theta, OD_z);
                m_hists->BookFillHist("DD/PositionCutPulseArea_Coincidence",2000,0,2000,120,0,120,(*m_event->m_odHGPulses)->pulseArea_phd[i],(*m_event->m_odHGPulses)->coincidence[i]);
            }
        }
    }

    // Look at all Skin pulses
    for (int i = 0; i < (*m_event->m_skinPulses)->nPulses; i++)
    {
        if ((*m_event->m_skinPulses)->coincidence[i] >= 3)
        {
            m_hists->BookFillHist("Skin/SkinPulseArea", 2000, 0.0, 2000, (*m_event->m_skinPulses)->pulseArea_phd[i]);
            m_hists->BookFillHist("Skin/SkinPulseAreaTight", 2000, 0.0, areaTight, (*m_event->m_skinPulses)->pulseArea_phd[i]);
            Skin_theta = (*m_event->m_skinPulses)->pulseTheta[i] * PI / 180.;
            Skin_z = (*m_event->m_skinPulses)->zPosition_mm[i];
            m_hists->BookFillHist("Skin/AllSkinCentroidPositions", 450, 0., 6.3, 450, -150, 300, Skin_theta, Skin_z);
        }
    }

    // // OD to SKIN correlation - just look at the largest pulshtopes in the events
    // if (ODMaxPulse != -1 && SkinMaxPulse != -1)
    // {
    //     float skinLargestPulseArea = (*m_event->m_skinPulses)->pulseArea_phd[SkinMaxPulse];
    //     int skinCoincidence = (*m_event->m_skinPulses)->coincidence[SkinMaxPulse];

    //     float odLargestPulseArea = (*m_event->m_odHGPulses)->pulseArea_phd[ODMaxPulse];
    //     int odCoincidence = (*m_event->m_skinPulses)->coincidence[ODMaxPulse];

    //     // Cut on skin pulses
    //     if ((skinLargestPulseArea > 2.5) && (skinCoincidence > 3))
    //     {
    //         // Timing
    //         // Timing, include AFT
    //         int ODMaxPulseStartTime_ns = (*m_event->m_odHGPulses)->pulseStartTime_ns[ODMaxPulse] + (*m_event->m_odHGPulses)->areaFractionTime5_ns[ODMaxPulse];
    //         int SkinMaxPulseStartTime_ns = (*m_event->m_skinPulses)->pulseStartTime_ns[SkinMaxPulse] + (*m_event->m_skinPulses)->areaFractionTime5_ns[SkinMaxPulse];
    //         // Timing, not including AFT
    //         //int ODMaxPulseStartTime_ns = (*m_event->m_odHGPulses)->pulseStartTime_ns[ODMaxPulse];
    //         //int SkinMaxPulseStartTime_ns = (*m_event->m_skinPulses)->pulseStartTime_ns[SkinMaxPulse];

    //         m_hists->BookFillHist("Timing/ODMaxPulseStartTime", nBins, minBinNS, maxBinNS, ODMaxPulseStartTime_ns);
    //         m_hists->BookFillHist("Timing/SkinMaxPulseStartTime", nBins, minBinNS, maxBinNS, SkinMaxPulseStartTime_ns);
    //         m_hists->BookFillHist("Timing2D/ODMaxPulseStartTimeVSSkin", nBins, minBinNS, maxBinNS, nBins, minBinNS, maxBinNS, ODMaxPulseStartTime_ns, SkinMaxPulseStartTime_ns);

    //         float ODTimeDivSkinTime = 0;

    //         // Divide OD time and Skin time to find correleation around 1.
    //         if ((ODMaxPulseStartTime_ns != 0) && (SkinMaxPulseStartTime_ns != 0))
    //         {
    //             ODTimeDivSkinTime = (float)ODMaxPulseStartTime_ns / (float)SkinMaxPulseStartTime_ns;
    //             m_hists->BookFillHist("Timing/ODMaxPulseTimeDivSkinMaxPuseTime", 500, -10, 10, ODTimeDivSkinTime);
    //         }

    //         // cut on the timing correlation between OD and Skin
    //         if (ODTimeDivSkinTime > 0.95 && ODTimeDivSkinTime < 1.05)
    //         {
    //             m_hists->BookFillHist("ODSkinTimeCorr/ODPulseArea", 2000, 0.0, 2000, odLargestPulseArea);
    //             m_hists->BookFillHist("ODSkinTimeCorr/ODPulseAreaTight", 2000, 0.0, areaTight, odLargestPulseArea);
    //             m_hists->BookFillHist("ODSkinTimeCorr/SkinPulseArea", 2000, 0.0, 2000, skinLargestPulseArea);
    //             m_hists->BookFillHist("ODSkinTimeCorr/SkinPulseAreaTight", 2000, 0.0, areaTight, skinLargestPulseArea);
    //             m_hists->BookFillHist("ODSkinTimeCorr/ODPulseAreaVsSkinPulseArea", 500, 0.0, 500, 500, 0.0, 500, odLargestPulseArea, skinLargestPulseArea);

    //             // plot Skin pulse centroid postioons
    //             Skin_theta = (*m_event->m_skinPulses)->pulseTheta[SkinMaxPulse] * PI / 180.;
    //             Skin_z = (*m_event->m_skinPulses)->zPosition_mm[SkinMaxPulse];
    //             m_hists->BookFillHist("ODSkinTimeCorr/SkinCentroidPositions", 450, 0.0, 6.3, 450, -250, 800, Skin_theta, Skin_z);

    //             // plot skin centroid postiions
    //             OD_theta = (*m_event->m_odHGPulses)->pulseTheta[ODMaxPulse] * PI / 180.;
    //             OD_z = zPosFromRow((*m_event->m_odHGPulses)->pulseZPosition[ODMaxPulse]);
    //             m_hists->BookFillHist("ODSkinTimeCorr/ODCentroidPositions", 450, 0., 6.3, 450, -150, 300, OD_theta, OD_z);

    //             // cut on coincident peaks and the hottest source tube in the OD
    //             // CSD3
    //             if (((OD_theta > 3.8) && (OD_theta < 4.6) && (OD_z > 50) && (OD_z < 100)))
    //             {
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/ODPulseArea", 2000, 0.0, 2000, odLargestPulseArea);
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/ODPulseAreaTight", 2000, 0.0, areaTight, odLargestPulseArea);
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/SkinPulseArea", 2000, 0.0, 2000, skinLargestPulseArea);
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/SkinPulseAreaTight", 2000, 0.0, areaTight, skinLargestPulseArea);
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/ODPulseAreaVsSkinPulseArea", 500, 0.0, 500, 500, 0.0, 500, odLargestPulseArea, skinLargestPulseArea);
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/SkinCentroidPositions", 450, 0.0, 6.3, 450, -250, 800, Skin_theta, Skin_z);
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/ODCentroidPositions", 450, 0., 6.3, 450, -150, 300, OD_theta, OD_z);
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/ODCentroidPositionsFinerBinning", 225, 0., 6.3, 225, -150, 300, OD_theta, OD_z);

    //                 // Plot
    //                 float pulseLadderPos = (*m_event->m_odHGPulses)->pulseLadderPosition[ODMaxPulse];
    //                 float pulseZPos = (*m_event->m_odHGPulses)->pulseZPosition[ODMaxPulse];
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/PMTLadderZ", 20, 0.5, 20.5, 6, 0.5, 6.5, pulseLadderPos, pulseZPos);

    //                 // find time difference for coincident events
    //                 float timeDiffODSkin = (float)ODMaxPulseStartTime_ns - (float)SkinMaxPulseStartTime_ns;
    //                 m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/TimeDiffODSkin", 500, -250, 250, timeDiffODSkin);

    //                 // cut on what we think is the 511keV peak in the OD and plot the time difference for these events - hopefully ignores the ?~2.1MeV Na22 peak which just comptons energy into OD or Skin
    //                 // to get a coincident event
    //                 if (odLargestPulseArea >= 70 && odLargestPulseArea <= 140)
    //                 {
    //                     m_hists->BookFillHist("CSD3/ODSkinTimeCorrAndPos/511Cut/TimeDiffODSkin", 500, -250, 250, timeDiffODSkin);
    //                 }
    //             }
    //         }
    //     }
    // }
}
// Finalize() - Called once after event loop.
void HB_OD_AmLi::Finalize()
{
    INFO("Finalizing HB_OD_AmLi Analysis");
    m_hists->GetHistFromMap("DD/PositionCutPulseArea")->GetXaxis()->SetTitle("pulseArea");
    m_hists->GetHistFromMap("DD/PositionCutPulseAreaTight")->GetXaxis()->SetTitle("pulseArea (phd)");
    m_hists->GetHistFromMap("DD/PositionCutODCentroidPositions")->GetXaxis()->SetTitle("Theta");
    m_hists->GetHistFromMap("DD/PositionCutODCentroidPositions")->GetYaxis()->SetTitle("ZPos");

    m_hists->GetHistFromMap("DD/PositionCutPulseArea_Coincidence")->GetXaxis()->SetTitle("pulseArea (phd)");
    m_hists->GetHistFromMap("DD/PositionCutPulseArea_Coincidence")->GetYaxis()->SetTitle("Coincidence");


    // THStack *hs = new THStack("hs", "Stacked Pulse Area");
    // //create three 1-d histograms
    // TH1F *h1st = new TH1F("h1st", "test hstack", 100, -4, 4);
    // h1st->FillRandom("gaus", 20000);
    // h1st->SetFillColor(kRed);
    // h1st->SetMarkerStyle(21);
    // h1st->SetMarkerColor(kRed);
    // hs->Add(h1st);
    // TH1F *h2st = new TH1F("h2st", "test hstack", 100, -4, 4);
    // h2st->FillRandom("gaus", 15000);
    // h2st->SetFillColor(kBlue);
    // h2st->SetMarkerStyle(21);
    // h2st->SetMarkerColor(kBlue);
    // hs->Add(h2st);
    // TH1F *h3st = new TH1F("h3st", "test hstack", 100, -4, 4);
    // h3st->FillRandom("gaus", 10000);
    // h3st->SetFillColor(kGreen);
    // h3st->SetMarkerStyle(21);
    // h3st->SetMarkerColor(kGreen);
    // hs->Add(h3st);
}

double HB_OD_AmLi::zPosFromRow(double row)
{
    double zPos_cm;
    zPos_cm = -70.0 * row + 319.046;
    return zPos_cm;
}