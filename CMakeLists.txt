find_package(ROOT)
find_library(ROOT_TREEPLAYER_LIBRARY TreePlayer HINTS ${ROOT_LIBRARY_DIR} REQUIRED)
find_library(ROOT_TXMLENGINE_LIBRARY XMLIO HINTS ${ROOT_LIBRARY_DIR} REQUIRED)

set (subproject_name HB_OD_AmLi)   # change the subproject name 

include_directories(
  "${CMAKE_CURRENT_SOURCE_DIR}/include"
  )

SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${ROOT_CXX_FLAGS}")
file(GLOB SOURCES "${PROJECT_SOURCE_DIR}/modules/${subproject_name}/src/*.cxx")

# Pay attention to the main program name
add_module(subproject_name ${subproject_name}Main.cxx ${SOURCES})

SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS}")

target_link_libraries(${subproject_name}
  AlpacaCoreLib
  ${ROOT_TARGETS}
  ${ROOT_TREEPLAYER_LIBRARY}
  ${ROOT_TXMLENGINE_LIBRARY} 
  ${RQ_LIB_TARGET} )

install(TARGETS ${subproject_name} DESTINATION bin)
